/*  1. Опишіть, як можна створити новий HTML тег на сторінці.
        метод createElement()

    2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
        insertAdjacentHTML(position, text) position - позиция согзданого елемнта

    3. Як можна видалити елемент зі сторінки?
        removeElement()



Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.


Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];

 */

function bringOnPage(array, parent = document.body) {
    let newArr = array.map(item=>`<li>${item}</li>`)
    parent.insertAdjacentHTML("beforeend", `<ul>${newArr.join(' ')}</ul>`)
}
console.log(bringOnPage(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], ))
