let eyeIcon = document.querySelectorAll('.icon-password');
let enterPassword = document.getElementById("firstPass");
let confirmPassword = document.getElementById("secondPass");
eyeIcon.forEach(item => {
    item.addEventListener('click', function (event) {
        if (event.target.classList.contains('fa-eye')) {
            event.target.classList.replace('fa-eye', 'fa-eye-slash');
        } else {
            event.target.classList.replace("fa-eye-slash", "fa-eye");
        }
    })
})
enterPassword.addEventListener('click',
    () => enterPassword.type === 'password' ? enterPassword.type = 'text' : enterPassword.type = 'password')
confirmPassword.addEventListener('click',
    () => confirmPassword.type === 'password' ? confirmPassword.type = 'text' : confirmPassword.type = 'password')

let warning = document.createElement("p");
let btn = document.querySelector('.btn');
warning.classList.add("warning_styles");

btn.addEventListener('click', function (event) {
    if(enterPassword.value !== confirmPassword.value) {
        console.log(enterPassword, confirmPassword)
        warning.innerText = "Please enter correct password";
        warning.style.color = "red";
        document.querySelector('#appendLabel').append(warning)
    } else {
        alert("You are welcome");
    }
})