/*  1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
        екранування використовується для того щоб ви могли юзати символи та лабки, для екранування використов - /

    2. Які засоби оголошення функцій ви знаєте?
        1) declaration
        2) expression
        3) arrow func

    3. Що таке hoisting, як він працює для змінних та функцій?
        hoisting - це процес доступу до змінних. Як я зрозумів в Джс компілятор проходить два рази по коду:
        в перший раз збирає змінні, а в другий запускає код, але для лет і конст хостінг працює по іншому від вар.
 */




function createUser() {
    let firstName = prompt('enter name')
    let lastName = prompt('enter lastname')
    let birthday = prompt('enter age dd.mm.yyyy')

    let newUser = {
        firstName,
        lastName,
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge() {
            let now = new Date().getTime()
            console.log(now)
            let birthInSec =  new Date(birthday.split('.').reverse().join()).getTime()
            return Math.round((now - birthInSec) / 3.154e+10)
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.split('.').reverse()[0]
        }
    }
    return newUser;
}

console.log(createUser().getLogin())
console.log(createUser().getAge())
console.log(createUser().getPassword())