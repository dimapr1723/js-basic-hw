/*  1. Опишіть своїми словами як працює метод forEach.
       метод forEach перебирає елементи масиву та приміняє для кожного елементу колбек функцію.

    2. Як очистити масив?
        метод pop() видаляє останній елемент масиву;
        метод shift() вадаляє перший елемент масиву;
        можно використати метод splice()

    3. Як можна перевірити, що та чи інша змінна є масивом?
        Array.isArray(arr) - перевірка на масив
 */


function filterBy(array, data) {
    const newArr = array.filter((e) => {
        if(typeof e !== data) return e ;
    })
    return newArr
}

// const filterBy = (array, data) => {array.filter(e => {if(typeof e !== data)})}

console.log(filterBy([1, '', true, 'aadasda', 2], 'number'))