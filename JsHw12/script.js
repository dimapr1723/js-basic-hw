let keys = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
    keys.forEach(item => {
        if (item.textContent.toLowerCase() === event.key.toLowerCase()) {
            item.classList.add('keyColor')
        } else {
            item.classList.remove('keyColor')
        }
    })
})

