/*
        1.Опишіть своїми словами що таке Document Object Model (DOM)
            DOM - це об'єктна модель документа, описує структуру сторінки,
            можна ствворювати елементи та події.


        2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
            innerHTML - повертає контент разом з хтмл тегом
            innerText - повертає тільки текст

        3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
            1) getElementBy(Id, classNane...)
            2) querySelector(".class", "#id")

           querySel - новіший метод і зараз майже завжди використовують його,
           але getElem повертає живу колекцію елеменів, а querySel статичну, це може бути корисно в різних випадках
*/


let p = document.body.querySelectorAll('p');


for(let par of p) {
    par.style.background = '#ff0000'
}
console.log(p)

let optionId = document.body.querySelector('#optionsList')
console.log(optionId)

console.log(optionId.parentElement)

console.log(optionId.children)

console.log(optionId.childNodes)

let testPar = document.querySelector('#testParagraph');

testPar.innerHTML = 'This is a paragraph';

let header = document.querySelector('.main-header');

for(let elem of header.children) {
    console.log(elem)
    elem.classList.add("nav-item")
}

let secElements = document.querySelectorAll('.section-title')

for(let removeClass of secElements) {
    removeClass.classList.remove('section-title')
}



