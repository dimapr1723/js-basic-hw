let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.tab-content')
let tabName;
console.log(tabsTitle)

tabsTitle.forEach(elem => {
    elem.addEventListener('click', () => {
        if(elem.classList.contains('active')) {
            elem.classList.toggle('active');
        } else {
            let clickedElements = document.querySelectorAll('.active');
            for(let clicked of clickedElements) {
                clicked.classList.remove('active')
            }
            elem.classList.add('active')
        }
        tabName = elem.getAttribute('data-tab');
        tabsContent.forEach(item => {
            item.classList.contains(tabName) ? item.classList.add('active-text') : item.classList.remove('active-text');
        })

    })
})





