/*  1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
        setTimeout() - дозволяє викликати функцію один раз через заданий проміжок часу
        setInterval() - дозволяє викликати функцію багато разів через заданий проміжок часу

    2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
        Функція буде працювати одразу після завершення роботи усього коду на сторінці

    3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
        Щоб припиняти подальші виклики які будуть нагружати сторінку


При запуску програми на екрані має відображатись перша картинка.
Через 3 секунди замість неї має бути показано друга картинка.
Ще через 3 секунди – третя.
Ще через 3 секунди – четверта.
Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
 */


let counter = 0;
const slider = () => {
    let images = document.querySelectorAll('.image-to-show');
    if(counter === 4) counter = 0;
    console.log(counter)
    images.forEach((item, index)  => {
        console.log(item)
        if(counter === index) {
            item.classList.add('show')
        } else {
            item.classList.remove('show')
        }
    })
    counter++;
}

let imageInterval = setInterval(slider, 1000)

let stopBtn = document.querySelector('.stop');
let continueBtn = document.querySelector('.start')
stopBtn.addEventListener('click', () => {
    clearInterval(imageInterval)
    continueBtn.removeAttribute('disabled')
})
continueBtn.addEventListener('click', () => {
    imageInterval = setInterval( slider, 1000);
    continueBtn.setAttribute('disabled', 'disabled')
})


    // images.forEach(item => {
    //
    // })
