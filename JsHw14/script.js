let themeBtn = document.querySelector('.theme-button');

themeBtn.addEventListener('click', () => {
    document.body.classList.toggle("dark");
    localStorage.setItem('theme', document.body.className)
})

document.body.className = localStorage.theme ;

